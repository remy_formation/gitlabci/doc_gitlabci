# Prérequis pour les ateliers gitlab

- Une VM pour héberger un runner et docker (documenté)
- Un compte gitlab.com (https://gitlab.com/users/sign_in)
- un compte Microsoft ou github pour VScode (https://github.com/login)
- VScode sur la VM ou votre poste (Documenté, je conseil de l'avoir directement sur la VM, comme ça vous pourrez partager le terminal avec les commandes docker en plus)


## Création de la VM

Créer une VM linux ubuntu x64 avec pour image, ubuntu 20.04 (la LTS)

En capacité :

RAM  : 4Go  (idéalement 6go)

Stockage : 20 Go ( 30Go Si possible)

Cpu : au mieux

**Faire une installation minimale pour limiter l'usage disque**

Autoriser les copié / collé :

- une fois la VM en cours exécution: Périphérique > Installer l’image CD des additions invités

- Redémarrer, puis une fois la VM en cours exécution : Périphérique >  Presse-papier partagé, choisir bidirectionnel

Je vous conseil d'installer en plus:

- git (apt-get install git)
- tree
- vim ou nano

## Installation de la ligne de commande pour les runners

Pour télécharger l'utilitaire de gestion des runners:

```bash
# Download the binary for your system
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Give it permission to execute
sudo chmod +x /usr/local/bin/gitlab-runner

# Create a GitLab Runner user
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Install and run as a service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
```

Pour la création des runners, on verra ça pendant la partie théorique et les tp !


## Installation de docker

```bash
 # Prérequis
 sudo apt-get update
 sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
    
# Docker GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# Ajout des repo docker
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    
# Installatin de docker:
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
 
# On ajoute docker à l'utilisateur courant
sudo groupadd docker
sudo usermod -aG docker $USER

# On reboot et c'est bon normalement, vous pouvez tester avec:
docker run hello-world
```


## Installation de VScode et git

Pour pouvoir faire des ateliers à plusieurs

### Sous Linux

```bash
# On ajoute le repo
sudo apt-get install wget gpg
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
rm -f packages.microsoft.gpg

# On dl vscode
sudo apt install apt-transport-https
sudo apt update
sudo apt install code # or code-insiders

# Pour git
sudo apt install git
```

Ensuite, on ouvre les extensions (Ctrl + shift + X) et on installe "LiveShare"

Installer aussi "Gitlab Workflow"

### Sur Windows

| :warning: | Seulement si pour une raison ou une autre vous n'avez pas le choix |
|---|---|

Je conseil en plus d'ajouter : https://gitforwindows.org/ pour gérer le répertoire avec une CLI type Linux

Pour VScode vous pouvez le récupérer ici : https://code.visualstudio.com/

Ensuite, on ouvre les extensions (Ctrl + shift + X) et on installe "LiveShare"

