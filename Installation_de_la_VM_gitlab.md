# Installation de la VM gitlab

Objectif du document : répertorier toutes les étapes nécessaires pour avoir une VM qui tourne avec GITLAB-CI hébergé dessus. On pourra l'utiliser pour les TP sur la partie administration.

On va créer une VM avec :

- Gitlab-CI d'installer
- L'installateur de runner

Note : Il faut aussi un compte microsoft ou github

note : idée de tp, création d'un gitlabCI, interfaçage avec du keycloak pour les utilisateurs, gestions secret toussa toussa

## Création de la VM

Créer une VM linux ubuntu x64 avec pour image, ubuntu 20.04 (la LTS)

En capacité :

RAM  : 6Go (4Go pour le runner, 2Go pour le reste, idéalement étendre à 8Go)

Stockage : 15 Go ( 20Go Si possible)

Cpu : au mieux

**Faire une installation minimale pour limiter l'usage disque**

Autoriser les copié / collé :

Périphérique > Installer l’image CD des additions invités

 redémarrer, puis dans Périphérique >  Presse-papier partagé, choisir bidirectionnel

# Installation de gitlabCI

Installation des dépendances:

```Bash
sudo apt-get update
sudo apt-get install -y curl openssh-server ca-certificates tzdata perl
```

Téléchargement du script d'installation du répertoire:

```bash
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
```

On installe en précisant son url. A noter que Nip.io est un domaine qui permet de faire de la redirection en local vers l'adresse ip indiqué. De plus, pour des soucis de simplicité, on ne va s'occuper des problèmes certificats pour le moment.

```bash
sudo EXTERNAL_URL="http://127.0.0.1.nip.io" apt-get install gitlab-ee
```

Attention, l'installation prend du temps !

| :warning: | La première installation va échouer. il s'emblerait que l'installateur gitlab à des problèmes : https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/6518 il faut donc recourir à une intervention manuelle en attendant |
| --------- | ------------------------------------------------------------ |

Correctif temporaire :


```bash
sudo vim /opt/gitlab/embedded/service/omnibus-ctl/lib/gitlab_ctl/pg_upgrade.rb

# ligne 28 remplacer :
@port = port || public_node_attributes['postgresql']['port']
# par :
@port = 5432 || public_node_attributes['postgresql']['port']

# On relance l'install
sudo EXTERNAL_URL="http://127.0.0.1.nip.io" apt-get install gitlab-ee -f
```

Le mdp root se trouve ensuite dans le fichier : 

```bash
sudo cat /etc/gitlab/initial_root_password
```

Connectez vous et rendez vous sur votre profil (Icone tout en haut à droite > Edit profil)

![password](C:\Users\Rémy\Desktop\Extia\formation\gitlabCI\images\password.png)

Puis dans l'onglet password, le mettre à jour (il faut le faire après l'installation car le mot de pass root par défaut expire au bout d'un court délai)

## Installations nécessaire pour les runners

Créer un groupe "racine" qui contiendra tout les projets du TP :

Depuis Menu > Groups > Create group

![create_groupe](C:\Users\Rémy\Desktop\Extia\formation\gitlabCI\images\create_groupe.png)

Renseigner le nom, peut importe la visibilité pour le moment, et vous pouvez ignorer les info de gitlab experience / invitation de membre.

Ensuite, dans le groupe, dans Settings > CI / CD il y a l'option "Show runner installations instructions"

```bash
# Download the binary for your system
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Give it permission to execute
sudo chmod +x /usr/local/bin/gitlab-runner

# Create a GitLab Runner user
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Install and run as a service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
```

Pour la création des runners, on verra ça pendant la partie théorique et les tp !

## Installation de VScode

Pour pouvoir faire des ateliers à plusieurs

### Sous Linux

```bash
# On ajoute le repo
sudo apt-get install wget gpg
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
rm -f packages.microsoft.gpg

# On dl vscode
sudo apt install apt-transport-https
sudo apt update
sudo apt install code # or code-insiders
```

Ensuite, on ouvre les extensions (Ctrl + shift + X) et on installe "LiveShare"

### Sur Windows



