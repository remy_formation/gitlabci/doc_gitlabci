# Exercice 3 : GitlabCI et docker

But de l'exercice:

- Aborder un autre exemple de pipeline d'intégration
- Utilisation de workflow
- Utilisation d'un runner Shell
- Présentation de schedule

Etapes clés:

- Créer un runner shell pouvant faire du docker
- Faire un build d'image
- Personnaliser la pipeline en fonction de la branche 
- Faire un Schedule

Temps estimé : 1h

## Créer un runner pouvant  faire un docker build

**Objectif: Etre en mesure de build et déposer une image docker sur un registre. Juste pour information, des outils comme buildah commencent à emmerger pour faire du docker build avec des droits plus limités**

=> Ajoutez les droits docker au compte "gitlab-runner"

```bash
sudo usermod -aG docker gitlab-runner
```

=> Suite à ça, créez un runner avec pour exécuteur "Shell".

=> Il y a un bug sur les runners gitlabCI tournant sur du ubuntu 20.04 (cf: https://gitlab.com/gitlab-org/gitlab-runner/-/issues/26605). En attendant, vous pouvez le contourner en supprimant / dépeçant un fichier spécifique 

```bash
sudo su gitlab-runner
cd
mv .bash_logout .bash_logout.bckp
```

**Etat des lieux:**

- **Vous avez runner prêt à faire du docker build**

## Faire son premier build d'image

=> Pour commencer, vous allez créer un nouveau projet

=> Le premier fichier à créer est le Dockerfile. Dans notre cas, nous allons utiliser une image classique pour faire un site web : httpd (aussi connu sous le nom d'apache).

```bash
FROM httpd:2.4-alpine

RUN echo "This is my super http_server" > /usr/local/apache2/htdocs/index.html
```

- Pour l'instant, on va simplement reprendre l'image en changeant l'index.html pour s'assurer qu'il s'agit bien de notre image.

=> Vous pouvez maintenant créer votre fichier .gitlab-ci.yml. Le but est de créer une image qui portera pour tag la branche en cours

Toolbox:

```bash
# Se connecter à un registre docker
docker login -u "username" -p "password" <url_du_registre>

# Lancer le build d'une image docker, pour rappel on veut que le tag soit le nom de la branche en cours
docker build -t "le_nom_de_mon_image:le_tag_de_mon_image"

# Upload une image docker
docker push "le_nom_de_mon_image:le_tag_de_mon_image"
```

Bien évidemment, vous pouvez vous appuyer sur la liste des variables gitlabCI pour vous simplifier la vie. (https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)

Et pour rappel, si vous avez besoins de leurs valeur, vous pouvez faire un "export" lors d'un job pour avoir la liste complète des variables renseignées.

=> Pour vérifier si c'est bon, vous pouvez télécharger l'image en local sur votre VM et l'éxécuter. Rendez vous dans la partie "Packages et Registries" pour retrouver l'url de l'image

=> Connectez vous au registry avec un docker login et un access token

```bash
docker login registry.gitlab.com
# username: oauth2
# password : un access token avec les droits "read registry"
```

=> Vous pouvez maintenant faire un docker pull de votre image:

```bash
docker pull registry.gitlab.com/<votre_groupe>/<un_sous_groupe>/<le_nom_de_votre_image>:<tag>
```

=> Dans le retour, vous pouvez voir le message "Image is up to date". Pourquoi ?

- Réponse : vous utilisez la même VM pour votre runner et pour tester. Comme l'exécuteur est du type shell, il utilise le même deamon docker que vous !

=> Vous pouvez finalement exécuter votre image :

```bash
# Le -p permet de faire du port forwarding, nécessaire pour accéder à l'application depuis votre navigateur
docker run -p 8080:80 registry.gitlab.com/<votre_groupe>/<un_sous_groupe>/<le_nom_de_votre_image>:<tag>
```

Pour valider ça, rendez vous sur firefox, et sur http://localhost:8080

**Etat des lieux:**

- **Vous avez une pipeline de build d'image docker**

## Personnaliser l'image en fonction de la branche

**Objectif : Modifier l'image docker ainsi que son tag en fonction de la branche**

=> Créez une nouvelle branche : dev

=> On veut maintenant personnaliser l'image en fonction de la branche sur laquelle elle est construite. Pour ça, créez un fichier index.html dans le répertoire git. Mettez dedans une chaine de caractère remarquable, ex: "Moi je viens de {{origin}}"

=> Mettez à jour votre Dockerfile pour remplacer le CMD par un COPY, ce qui va vous permettre d'insérer le fichier index.html dans l'image docker

```bash
COPY index.html /usr/local/apache2/htdocs/index.html
```

=> Maintenant, dans .gitlab-ci.yml, modifiez votre job pour remplacer la chaine de caractère remarquable par le nom de la branche. Je vous conseil de faire ça en pré-traitement du script.

Toolbox:

```bash
# templacer une chaine de caractère dans un fichier
sed -i "s/<ma_chaine>/<ma_nouvelle_chaine>/g" mon_fichier.txt
```

=> Vérifiez sur votre image que la substitution est bien faite (attention au cache internet de firefox, je vous conseil de faire un CTRL + F5 si la page index.html semble ne pas avoir était modifié).

=> Faite un merge vers le main, confirmez que l'image "main" est elle aussi customisée.

**Etat des lieux:**

- **Votre image est maintenant personnalisée en fonction de votre branche**

## Allez un peu plus loin dans la personnalisation 

**Objectif : Personnaliser un peu plus l'image docker en fonction de la branche. Dans le cadre générale, la dernière version stable (donc disponible sur le main) porte le tag "latest". On va en plus rajouter une variable dans le fichier index.html qui changera en fonction de la branche.**

=> Retournez sur la branche dev. Modifiez index.html pour rajouter une ligne avec une variable ex: "et j'ai avec moi la variable my_super_valeur qui vaut {{my_super_valeur}}"

=> Identifiez le mot clé qui permet de faire un traitement spécifique avant la création de la pipeline. C'est un des keywords global (liste des keyword https://docs.gitlab.com/ee/ci/yaml/). 

=> Modifiez votre .gitlab-ci.yml:

- rajoutez le "sed" supplémentaire qui modifie la valeur de votre nouvelle variable
- ajoutez à l'échelle globale les règles suivante :
  - Si ma branche est "main", je défini deux variables : docker_tag = "latest" et my_super_valeur = "pas un clou"
  - Dans les autres cas, docker_tag = la branche ou le tag qui a appelé la pipeline et my_super_valeur = "des clopinettes" 

- Remplacez toutes les variables qui utilisent directement le nom de la branche par la variable  "docker_tag". 

=> Vérifiez maintenant que votre nouvelle image dev est mise à jour sur votre VM.

=> Faite un merge, vérifiez dans le registry que l'image avec le tag "main" n'a pas bougé, mais que l'image "latest" est mise à jour. Testez là aussi sur votre VM.

=> Enfin, vous pouvez faire un tag git du main, et vérifier que votre CI fonctionne aussi dans ce cas et produit bien une image avec le tag git équivalent.

**Etat des lieux:**

- **Vous avez maintenant une gestion similaire à ce que vous pourriez faire pour du multi environnement**

## Avez vous déjà entendu parlé du nightly build ?

**Objectif : Mettre à disposition une image "nightly". La version nightly correspond normalement à une version build la nuit à partir de la branche de développement. C'est donc une version potentiellement instable, mais qui contient les dernières mise à jour**

=> Rendez vous dans CI/CD > Schedules

=> Cette section permet de build automatiquement non pas sur un évenement sur le répertoire git, mais sur un timer. Créez une nouvelle tâche programmée, toutes les 5 minutes, sur la branche dev, en prenant soin de remplacer la variable docker_tag par "nightly"

Toolbox:

```bash
# Pour des raisons évidentes, je vais pas vous demander de faire la nightly toute les nuites ;)
#Expression cron pour toutes les 5 minutes:
*/5 * * * *
```

Pour des raisons technique, gitlab ne va pas réellement faire le schedule toutes les 5mins (pas avec l'offre gratuite en tout cas ). Par contre, vous pouvez forcer l'éxécution à la main pour vérifier que ça fonctionne bien

| :warning: | Pensez à désactiver votre schedule nightly une fois testé, ça serait dommage de consommer des ressources gitlab.com pour rien |
| --------- | ------------------------------------------------------------ |



**Etat des lieux:**

- **Vous avez maintenant un tag nightly mis à disposition**

## Conclusion

L'exercice vous a permis de voir une autre manière de construire une pipeline, avec une autre technologie. Dans notre exemple, la variable qui dépend de la branche n'a pas vraiment de sens. Mais en pratique, on pourrait utiliser un mécanisme similaire avec une branche par environnement, et la variable serait une variable lié à l'environnement, comme : l'url d'un serveur qui servirait à l'application, des options de configuration jvm, des paramètres à renseigner dans une CLI...

Si vraiment vous voulez du rab : Faites un test unitaire à base de docker run et de Curl. 
