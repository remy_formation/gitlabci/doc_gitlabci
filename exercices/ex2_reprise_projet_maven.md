# Exercice 2 : Reprise du projet Maven

But de l'exercice:

- Etre plus à l'aise avec les notions de base de gitlabCI : access token, merge request, issues, stages, jobs, registry...
- Appliquer les principaux keyword de gitlabCI

Etapes clés:

- Faire un fork du projet
- Préparer les issues
- Tests unitaires
- Sécuriser l'upload d'artefacts
- Ajouter des tags git

Temps estimé : 1h30

## Faire un fork du projet

**Objectif: Récupérer le projet dans votre espace de travail**

=> Rendez vous sur : https://gitlab.com/remy_formation/gitlabci/exemple_maven et faites un fork. Pour ça utilisez l'option préssente en haut à droite

![ex2_fork](..\img_ex\ex2_fork.png)

**Etat des lieux:**

- **Pour information, un fork est une copie détaché du répertoire d'origine qui peut évoluer librement et différemment du repo d'origine**

## Préparer les issues

**Objectif: Préparer les objets nécessaire pour suivre l'avancement du projet**

=> Dans la partie "Project Information", créez les labels : "InProgress", "validation", "securite" et "versionning"

=> Dans la partie Issues de gitlabCI, préparez le tableau de bord en ajoutant une liste pour tracer les issues "InProgress", placez là entre Open et Closed 

=> Créer les issues (vous n'êtes pas obligé d'ajouter la description):

- Tests unitaires, avec le label "InProgress" car vous allez commencer par celle-ci ainsi que le label "validation"

"Je veux que les tests unitaires soient joués avant d'envoyer le package sur le registry. Aussi, je veux pouvoir télécharger facilement le résultat des tests."

- Sécuriser l'upload des artéfacts, avec le label "securité"

"Je veux qu'uniquement la branche main permette de déposer un package en version release. Les packages Snapshot peuvent être déposé depuis n'importe quelle branche."

- Ajouter des tags git, avec le label "versionning"

"Quand un package est déposé sur le registry, je veux qu'un tag git soit automatiquement apposé. Que ça soit un package Snapshot ou Release."

**Etat des lieux:**

- **Vous avez un dashboard  avec vos issues et vous pouvez traquer l'issue en cours**

## Tests unitaires

**Objectif : Rajouter l'exécution des tests unitaires**.

=> Depuis le repository, créez une branche "test_unitaire" 

=> Sur votre VM, ouvrez VScode et préparez votre espace de travail (workspace)

Dans un terminal :

```bash
code
```

Allez dans File > Open Folder...

Créez un dossier (en haut à droite de l'explorateur) "tp_gitlabci" et sélectionnez le.

=> Clonez le répertoire sur votre VM. Vous trouverez l'URL de votre répertoire sur la Page Repository > Files de gitlabCI. 

Sur un terminal (ouvert via VScode dans Terminal > New Terminal )

Pour nous faciliter la vie lors de TP et éviter d'avoir à saisir le mot de passe à chaque fois sans faire de clé SSH on va utiliser :

```bash
remy@remy-VirtualBox:~/tp_gitlabci$ git config --global credential.helper cache
remy@remy-VirtualBox:~/tp_gitlabci$ git config --global credential.helper 'cache --timeout=7200'
```

Ensuite, depuis notre nouveau dossier, on peut cloner le projet:

```bash
remy@remy-VirtualBox:~/tp_gitlabci$ git clone https://gitlab.com/xxx/xxx.git
```

Attention VScode vous demandera vos options d'authentification en haut dans la barre de commandes plutôt que dans le terminal : 

![ex2_git_clone](..\img_ex\ex2_git_clone.png)

=> Mettez vous sur la branche de développement

```bash
#cd votre_nom_de_repertoire/
cd exemple-maven-correction/ 
git checkout tests_unitaires
```

=> Ajoutez un stage de test, et le job associé pour effectuer les 

- Pour information, la commande maven pour exécuter les tests : "mvn test". 
- Si vous avez le plugin Gitlab Workflow, vous pouvez valider votre pipeline. A tout moment, vous pouvez valider votre fichier .yml si vous avez installé gitlab workflow sur VScode : Ctrl + Shift + P, "Gitlab: Validate Gitlab CI Config"

=> Une fois votre .gitlab-ci.yml mis à jour, vous pouvez faire un git add/commit/push et vérifier le bon fonctionnement sur gitlab-CI

Commandes git:

```bash
remy@remy-VirtualBox:~/tp_gitlabci/exemple-maven-correction$ git add .gitlab-ci.yml 
remy@remy-VirtualBox:~/tp_gitlabci/exemple-maven-correction$ git commit -m "Ajout des tests unitaires"
remy@remy-VirtualBox:~/tp_gitlabci/exemple-maven-correction$ git push --set-upstream origin tests_unitaires # Note : les fois suivantes vous pourrez utiliser git push uniquement
```

Output de votre nouveau job attendu :

```bash
# Après une série de downloading infini...
-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running com.remy.group.AppTest
Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.06 sec
Results :
Tests run: 1, Failures: 0, Errors: 0, Skipped: 0
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  11.768 s
[INFO] Finished at: 2022-03-29T18:52:55Z
[INFO] ------------------------------------------------------------------------
Cleaning up project directory and file based variables
00:00
Job succeeded
```

=> Le résultat est satisfaisant, mais l'issue indique que je dois pouvoir télécharger . Rajouter le nécessaire au job pour télécharger le fichier de résultat. 

- Pour information, maven va générer les rapports à l'emplacement : target/surefire-reports/ du répertoire de travail.

=> Une fois que c'est bon, vous pouvez conclure l'issue. Pour ça, faites une merge request. N'oubliez pas de citer le ticket dans celle-ci

=> Une fois faite, vous pouvez fermer votre ticket et lui enlever le tag "InProgress"

**Etat des lieux:**

- **Vous avez maintenant un stage et un job supplémentaire pour les tests unitaires**
- **Vous êtes en mesure de télécharger les résultats sous forme artéfacts**

## Sécuriser l'upload des artéfacts

**Objectif: Empêcher de déployer et écraser des packages dans certaines conditions. Je veux qu'uniquement la branche main permette de déposer un package en version release. Les packages Snapshot peuvent être déposé depuis n'importe quelle branche.**. 

=> Rendez vous sur le dashboard pour vous assigner la tâche suivante : "Sécuriser l'upload des artéfacts".

=> Créez une branche "securisation_upload" sur gitlabCI

=> Sur VSCode, récupérez la branche et mettez vous dessus

```bash
remy@remy-VirtualBox:~/tp_gitlabci/exemple-maven-correction$ git pull
remote: Enumerating objects: 1, done.
remote: Counting objects: 100% (1/1), done.
remote: Total 1 (delta 0), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (1/1), 284 bytes | 284.00 KiB/s, done.
From https://gitlab.com/remy_formation/exemple-maven-correction
   339cb52..e63baf3  main       -> origin/main
 * [new branch]      securisation_upload -> origin/securisation_upload
Already up to date.
remy@remy-VirtualBox:~/tp_gitlabci/exemple-maven-correction$ git checkout securisation_upload
Branch 'securisation_upload' set up to track remote branch 'securisation_upload' from 'origin'.
Switched to a new branch 'securisation_upload'
```

=> Dans un premier temps, ajouter un "pré-traitement" au job de push pour faire échouer systématiquement le job, puis faite un push

- Tout code retour autre que 0 sera considéré comme une erreur par gitlabCI, sauf indications contraire

=>  Dans un second temps, modifier le pré-traitement pour faire échouer le job à condition que la version soit une release. N'oubliez pas de tester si ça fonctionne dans les deux cas.

- Pour tester, pensez à passer votre version en 1.0 dans le fichier pom.xml, dans la balise projet. N'oubliez pas de l'ajouter à votre commit git

- Tool box: 

```bash
# Extraire le numéro de version
version=$(mvn org.apache.maven.plugins:maven-help-plugin:3.1.0:evaluate -Dexpression=project.version -q -DforceStdout)
# retourne 1.0-SNAPSHOT 
  
# Identifier facilement si c'est snapshot ou pas (pas hyper stable,  mais suffisant pour le TP)
release=1.0
echo ${release#*-}
1.0
echo ${version#*-}
SNAPSHOT
 
# On mix
version="1.0-SNAPSHOT"
if [ ${version#*-} != "SNAPSHOT" ]
then
        echo "C'est une SNAPSHOT"
else
        echo "C'est une RELEASE"
fi
```

- Yaml script multi ligne:

```yml
my_field:
  - |
    echo "command 1"
    echo "command 2"
```

=> Maintenant, ajoutez une condition sur la branche main. Il existe une variable prédéfini qui va vous aider : https://docs.gitlab.com/ee/ci/variables/predefined_variables.html. En formulant en français:

- Si la version est une Snapshot => OK dans tout les cas
- Sinon

  - Si la branche est "main" => OK
  - Sinon => Erreur


=> Enfin, c'est un peu sévère de créer une erreur sur une tentative de push lors de la publication d'une release sur une branche quelconque, transformez l'erreur en warning. Verifiez bien que :

- Si vous buildez une snapshot sur la branche de développement, c'est bon
- Si vous buildez une release sur la branche de développement, c'est en warning
- Pour tester le cas du release et main, on croisera les doigts lors de la merge request

=> Faire une merge request (idéalement positionnez votre version de pom.xml à 1.0  dans la branche avant le merge, pour vérifier qu'il n'y a pas de soucis ), n'oubliez pas de référencer l'issue dans la MR et si tout est bon, fermez le ticket !

**Etat des lieux:**

- **En plus des test unitaires, on a maintanant une vérification pour s'assurer qu'on ne va pas déposer un package en version "release" hors de la branche main**
- **Si vous vous demandez pourquoi on a pas utilisé rules : il nous fallait la valeur de la version du projet pour savoir si oui ou non on déposait le package. Comme rules s'évalue à l'instanciation de la pipeline, on a pas l'information à ce moment là**

## Ajouter des tags git 

**Objectif: Ajouter des tags git pour pouvoir facilement récupérer le code source associé à un package**

=> Rendez vous sur le dashboard pour récupérer la dernière tâche. Préparez une branche de développement appelée "tag"

=> Avant de commencer à modifier la pipeline il va vous falloir un token pour utiliser l'api de gitlab. Pour ça rendez vous dans les options de votre profil et générez un "Access Tokens". Les droits nécessaires sont : "api" et "write_repository". Attention, le token ne sera affiché qu'une seule fois à l'écran, notez le bien.

=> Mettez le token à disposition  sous forme d'une variable au niveau du groupe (via les configuration). Pensez bien à masquer sa valeur pour éviter qu'elle s'affiche lors de vos pipelines. 

=> Vous pouvez maintenant modifier votre .gitlab-ci.yml pour rajouter le stage de tag, ainsi que le job associé. Utilisez la version disponnible dans le pom.xml comme tag. Pensez aussi à remettre votre version en snapshot dans le pom.xml, sinon vous ne pourrez pas build.

- toolbox

```bash
# Pour faire un push de tag depuis une pipeline CI
git config --global user.email <email>
git config --global user.name <name> 
git tag -a -f -m "Tag by CI" $version
git remote set-url origin https://oauth2:<access_token>@gitlab.com/remy_formation/exemple-maven-correction.git
git push orgin -f --tags

# Afficher la liste des variables d'environnement gitlab-CI... ça peut servir pour le mail et le nom :)
export
```

=> Si tout se passe ~~bien~~ mal, vous devriez avoir des pipelines qui se lance à l'infini : votre commit déclenche une pipeline, qui fait un tag, qui déclenche une pipeline, qui fait un tag etc... Pour éviter le phénomène de "rebond" vous devez empécher la pipeline de se créer dans le cas où se qui déclenche la pipeline commence par une série de chiffres.

- toolbox

```bash
# regex pour détecter si la chaine de caractère ressemble à
# 1.0
# 17.5-SNAP
# 128.174.147.485.658
/^(\d+\.?)+/
```



**Etat des lieux:**

- **Vous avez une gestion des tag git (note, d'autres méthodes étaient possible, comme déployer le package release uniquement sur tag)**

## Conclusion

A travers l'éxercice, vous avez normalement revu la majorité des conceptes évoqués lors de la partie théorique, et ceux les plus utilisés dans un cadre réel. Vous avez aussi vue un exemple de pipeline un peu plus complexe. L'exemple n'est pas parfait et souffre de défauts encore à corriger (par exemple : le cas ou la release est annulé car sur une branche de développement) et d'évolutions possible (détection des tags plus robuste, laisser les merge request build et test mais pas publier) mais donne un bon aperçu de se qui peut être attendu.



