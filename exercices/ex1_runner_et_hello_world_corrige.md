# Exercice 1 : Préparation du groupe, installation du runner et hello world

But de l'exercie:

- faire un petit peu d'administration en créant un groupe.
- Ajouter votre éventuel binôme et votre humble formateur. 
- Installer un runner pour le groupe. 
- Enfin on terminera par tester si le runner fonctionne avec un projet "hello world".

Etapes clés:

- Créer un groupe
- Déployer un runner
- Créer un hello world

Temps estimé : 45 min

## Création du groupe

**Objectif: Créer le projet sur gitlab.com, ajouter votre binôme**.

=> Parcourir les menu pour retrouver l'option de création du groupe

![ex1_create_group](..\img_ex\ex1_create_group.png)

=> Mettez le nom de votre choix, une URL va vous être proposé. Vous pouvez laisser le groupe en Private ou Public. 

![ex1_create_group](..\img_ex\ex1_create_group2.png)

=> Vous pouvez maintenant inviter votre binome à rejoindre le groupe gitlabCI. Mettez le role à "Owner" (le même que celui qui à crée le groupe). N'oubliez pas d'ajouter votre formateur préféré :)

Pour ça, rendez vous dans la partie "Activity > Members"

Vous aurez l'option d'inviter des membres en haut à droite.

![ex1_add_user](..\img_ex\ex1_add_user.png)

**Etat des lieux:**

- **Vous avez maintenant un groupe dans lequel créer les différents projets pour la suite des TP**

## Déployer un runner

**Objectif : Ajouter un runner au groupe, pour prendre en charge vos pipelines**

=> Depuis la console de votre VM (idéalement ouverte dans VScode en partagé), utilisez la commande gitlab-runner pour instancier votre premier runner docker. Mettez un tag "reconnaissable" (ex: runner_formation) pour éviter de tomber sur les runners de gitlabCI

![ex1_token_runner](..\img_ex\ex1_token_runner.png)

On récupère le token dans la partie CI/CD de gitlabCI puis on peut faire le register:

![ex1_register_runner](..\img_ex\ex1_register_runner.png)

Dans mon cas:

- J'ai mis l'url
- J'ai mis le token
- en tag : docker et my_super_tag
- Pas d'instructions
- en executeur docker
- image de base maven:3.8.4-openjdk-17

**Etat des lieux:**

- **On a maintenant un runner capable de faire tourner des images docker pour notre code**
- **Grace au runner, on va pourvoir faire un projet avec un .gitlab-ci.yml**

## Créer un hello world

**Objectif : créer un projet de test pour vérifier que le runner fonctionne**

=> On fait fonctionner la mémoire, c'est dans les slides :)... Plus sérieusement, créez un projet (hello-world) depuis l'interface de gitlab. Attention à bien le créer dans le groupe.

![projet_01_groups](..\images\projet_01_groups.png)



![projet_02_new_projet](..\images\projet_02_new_projet.png)

![projet_03_init_du_repo](..\images\projet_03_init_du_repo.png)

=> Ensuite, vous pouvez soit le cloner en local sur vscode, le modifier et le push (un peu plus sympa en binôme) ou utiliser l'éditeur de pipeline proposé par gitlabCI. Le but est de créer un job qui fait soit "echo hello-world" soit un "maven --version". On oublie pas les règles de base : un stage, un job, on choisi le tag, le script, éventuellement une image docker...

![projet_20_pipeline_editor](..\images\projet_20_pipeline_editor.png)

=> Vérifier dans la partie CI/CD que ça se passe bien

![projet_21_hello_world](..\images\projet_21_hello_world.png)

**Etat des lieux:**

- **Votre première pipeline s'est exécuté**

- **Exercice fini ! vous avez tout ce qu'il vous faut pour continuer vers des exercices plus compliqués**